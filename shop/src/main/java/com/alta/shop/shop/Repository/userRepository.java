package com.alta.shop.shop.Repository;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.alta.shop.shop.Models.*;


@Repository
public  interface UserRepository extends JpaRepository<Users, Integer>  {

    public Users findByUserIdAndPasswordHash(int id,String pasString);
    public Users findByEmail(String email);
    public Users findByMobile(String phone);


}