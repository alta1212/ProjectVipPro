package com.alta.shop.shop.Service;


import java.util.List;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alta.shop.shop.Models.*;
import com.alta.shop.shop.Repository.*;
@Service

public class ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductImageRepository productimgRepository;
    @Autowired
    private CartRepsitory cartRepsitory;
    
    public List<Product[]> findByCategory(Category ca)
    {
       
        return productRepository.findByCategory(ca);
    }
    public CartItem addToCartItem(CartItem cart,int idUser)
    {
        cart.setUser(userRepository.findById(idUser).orElseThrow(
            () -> new UsernameNotFoundException("Product not found with id : " + idUser)
        ));
        cartRepsitory.save(cart);
        return null;
    }
    public Product findBySlug(String Slug)
    {
        return  productRepository.findBySlug(Slug);
    }
    public Product Create(Product pro)
    {
        return productRepository.saveAndFlush(pro);
    }
    public Product update(Product pro)
    {
        return productRepository.save(pro);
    }
    public void updateImage(ProductImage img)
    {
        productimgRepository.save(img);
    }
    public Product findById(int id)
    {
        return productRepository.findById(id).orElseThrow(
            () -> new UsernameNotFoundException("Product not found with id : " + id)
        );
    }
}
