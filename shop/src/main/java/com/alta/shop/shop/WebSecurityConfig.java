 package com.alta.shop.shop;
// import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


import com.alta.shop.shop.jwt.JwtRequestFilter;
 

@Configuration
public class WebSecurityConfig {
 

	

	// @Bean
    // public JwtRequestFilter JwtRequestFilter() {
    //     return new JwtRequestFilter();
    // }



	@Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
     
						http.csrf().disable()
						// dont authenticate this particular request
						// .authorizeRequests().
						
						// antMatchers("/api/**").permitAll()
                       // .

						// antMatchers("/api/**/auth/**")
						// .hasRole("ADMIN").anyRequest()
						// .authenticated().
						// // all other requests need to be authenticated
						// and()
						;
 
       // http.addFilterBefore(JwtRequestFilter(), UsernamePasswordAuthenticationFilter.class);
        http.headers().frameOptions();
		return http.build();
    }
 
 
}