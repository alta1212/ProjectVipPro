package com.alta.shop.shop.Service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.alta.shop.shop.Models.*;
import com.alta.shop.shop.Repository.*;
@Service

public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    String emailExists="Email allready exists";
    String phoneNumberExitsts="Phone number allready exists";

    @Override
    public UserDetails loadUserByUsername(String id) {
            Users user = userRepository.findById(Integer.parseInt(id)).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + id)
            );

            return new CustomUserDetails(user);
    }

    public Users login(Users u)
    {
        Users a=userRepository.findByUserIdAndPasswordHash(u.getUserId(), u.getPasswordHash());
        System.out.print(a);
        return a;
    }
    public Object register(Users u)
    {
        Users email=userRepository.findByEmail(u.getEmail());
        Users phone =userRepository.findByMobile(u.getMobile());
        if(email==null && phone ==null )
        {
          u=  userRepository.save(u);
        }
        else if(email!=null)
        return emailExists;
        else if (phone!= null)
        return phoneNumberExitsts;

        return u;
    }
}
