package com.alta.shop.shop.Models;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.List;

import lombok.*;

@Entity
@Data
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "categoryId")

    private int categoryId;
    @Column
    private int parentId;

    @Column(length=100)
    private String metaTitle;

    @NotNull(message = "slug name may not be null")
    @Column(length=200,unique = true)
    private String slug;

    @Column(length=400)
    private String title;

    @Column(length=400)
    private String text;


    @OneToMany(mappedBy = "category",cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Product> product;
}

