package com.alta.shop.shop.Controller;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alta.shop.shop.Models.*;
import com.alta.shop.shop.Repository.*;
import com.alta.shop.shop.Service.*;
import com.alta.shop.shop.Service.ProductService;
import com.alta.shop.shop.Tools.Tools;
import com.alta.shop.shop.jwt.JwtTokenUtil;
import org.springframework.web.server.ResponseStatusException;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
@RestController
@RequestMapping("/api/user/")
public class UserController {
    HttpStatus status;
    @Autowired

    Tools tool;
    @Autowired
    private UserService uService;
    @Autowired
    private JwtTokenUtil tokenProvider;
    
    @PostMapping("/login")
    public Users authenticateUser(@RequestBody Users  users) {
      //  users.setPasswordHash(tool.encryption(users.getPasswordHash())); 
      
        users=uService.login(users);
       if(users!=null)
       {
        // Trả về jwt cho người dùng.
        String jwt = tokenProvider.generateToken( users);
        users.setToken(jwt);
        return users;
       }
       throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Wrong username or password");
     
        
    }
  
    @PostMapping("/register")
    public ResponseEntity<Users> register(@RequestPart("info") Users users ,@RequestParam("file") MultipartFile file ) {
 
        try
        {
            String uuid = UUID.randomUUID().toString();
            String filename = "user\\"+uuid+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            tool.uploadFile(file, filename);
            users.setImage(filename);
         //   users.setPasswordHash(tool.encryption(users.getPasswordHash()));
            users=  (Users) uService.register(users); 
            return new ResponseEntity(users, HttpStatus.CREATED);

        }
        catch(Exception e)
        {
            String error=(String) uService.register(users);
            throw new ResponseStatusException(HttpStatus.CONFLICT,error);   
        }
       




    }
  
  
  
}
