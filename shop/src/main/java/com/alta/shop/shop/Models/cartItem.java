package com.alta.shop.shop.Models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.*;

@Entity
@Data
@Table(name = "cartItem")
public class CartItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cartItemId")

    private int cartItemId;


    @NotNull(message = "productId may not be null")
    @Column(length=100)
    private int productId;


    @NotNull(message = "Price  may not be null")
    @Column(length=100)
    private Float price;

    @NotNull(message = "quantity number may not be null")
    private int quantity;

 
    private String active;

    @NotNull(message = "createdAt may not be null")
    @Column(length=100)
    private String createdAt;

    @NotNull(message = "updatedAt date may not be null")
    @Column(length=400)
    private String updatedAt;


   


    @ManyToOne
    @JsonIgnore
    private Users user ;
}

