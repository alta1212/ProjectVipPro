package com.alta.shop.shop.Tools;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.text.Normalizer;
import org.springframework.web.multipart.MultipartFile;

import com.alta.shop.shop.Models.Users;

import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

import java.io.InputStream;
import java.nio.file.*;
@Service

public class Tools {
    @Autowired
    private Environment env;
    
    private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");
    private static final Pattern EDGESDHASHES = Pattern.compile("(^-|-$)");
    int iterations = 200000;  // number of hash iteration
    int hashWidth = 256;      // hash width in bits


    private final Path rootLocation;
    Tools() {

        this.rootLocation = Paths.get("src\\main\\resources\\upload");

    }
    public String encryption (String password)
    {

        String passHash=env.getProperty("password.hash");  // secret key used by password encoding
        Pbkdf2PasswordEncoder pbkdf2PasswordEncoder =
        new Pbkdf2PasswordEncoder(passHash, iterations, hashWidth);
        pbkdf2PasswordEncoder.setEncodeHashAsBase64(true);
        String encodedPassword = pbkdf2PasswordEncoder.encode(password);
        return encodedPassword;
    }
    public Boolean deencryption (String password, Users u)
    {
        
        String passHash=env.getProperty("password.hash");  // secret key used by password encoding
        Pbkdf2PasswordEncoder pbkdf2PasswordEncoder =
        new Pbkdf2PasswordEncoder(passHash, iterations, hashWidth);
        return pbkdf2PasswordEncoder.matches(password,u.getPasswordHash());
       
    }
    public String createSlug(String input)
    {
        
        String uuid = UUID.randomUUID().toString();

        String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        slug = EDGESDHASHES.matcher(slug).replaceAll("")+"-"+uuid;
        return slug.toLowerCase();
        
    }
    public String uploadFile(MultipartFile file,String filename) {
        try {
            if (file.isEmpty()) {
                System.out.println("File is nulll");
            }
            
            try (InputStream inputStream = file.getInputStream()) {
               
                Files.copy(file.getInputStream(), this.rootLocation.resolve(filename),
                StandardCopyOption.REPLACE_EXISTING); 
                System.out.println("ok");
                        return "ok";
            }
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("error");
        }
        return null;
    }
}
