package com.alta.shop.shop.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.http.ResponseEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.alta.shop.shop.Models.*;
import com.alta.shop.shop.Repository.*;
import com.alta.shop.shop.Tools.*;

import com.alta.shop.shop.Service.*;

@RestController
@RequestMapping("/api/product/")
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    Tools sys ;
    @GetMapping("category/{id}")
    public List<Product[]> a(@PathVariable("id") int  id)
    {
        Category aa = new Category();
       aa.setCategoryId(id);
       return productService.findByCategory(aa);
       
    }
    @GetMapping("{slug}")
    public Product getBySlug(@PathVariable("slug") String  slug)
    {
        return productService.findBySlug(slug);
    }
    @PostMapping(value = "auth/create")
    public Product CreateProductByFile(
         @RequestParam("idCategory") int idCategory
        ,@RequestPart("info") Product product 
        ,@RequestParam("file") MultipartFile[] file
        ) {
        String uuid = UUID.randomUUID().toString();

        Category cate =categoryService.getByid(idCategory);
        // String filename = "product\\"+uuid+"\\"+file[i].getOriginalFilename();
        // product.setImg1("product\\"+uuid+"\\"+file[0].getOriginalFilename());
        // product.setImg2("product\\"+uuid+"\\"+file[1].getOriginalFilename());
        // product.setImg3("product\\"+uuid+"\\"+file[2].getOriginalFilename());
        // product.setImg4("product\\"+uuid+"\\"+file[3].getOriginalFilename());
        // product.setImg5("product\\"+uuid+"\\"+file[4].getOriginalFilename());
        product.setSlug(sys.createSlug(product.getTitle()) );
        product.setCategory(cate);
        Product pro=productService.Create(product);
        for(int i =0;i<file.length;i++)
        {
            ProductImage img = new ProductImage();
            img.setImg("product/"+uuid+file[i].getOriginalFilename());
            img.setProduct(pro);
            productService.updateImage(img);

            sys.uploadFile(file[i],"product/"+uuid+file[i].getOriginalFilename());

        }
         
        

        
        return pro;
    }
    @PutMapping("auth/update/{id}")
    public Product updateProductByFile(
        @RequestParam("idCategory") int idCategory
        ,@RequestPart("info") Product product 
        ,@RequestParam("file") MultipartFile[] file,
        @PathVariable("id") int  id
        ) {
        Category cate =categoryService.getByid(idCategory);

        Product pro= productService.findById(id);
        String uuid = UUID.randomUUID().toString();
        // product.setImg1("product\\"+uuid+"\\"+file[0].getOriginalFilename());
        // product.setImg2("product\\"+uuid+"\\"+file[1].getOriginalFilename());
        // product.setImg3("product\\"+uuid+"\\"+file[2].getOriginalFilename());
        // product.setImg4("product\\"+uuid+"\\"+file[3].getOriginalFilename());
        // product.setImg5("product\\"+uuid+"\\"+file[4].getOriginalFilename());
        for(int i =0;i<file.length;i++)
        {
            sys.uploadFile(file[i],"product/"+uuid+"/"+file[i].getOriginalFilename());

        }
        product.setCategory(cate);
     
    
        pro.setTitle(product.getTitle());
        pro.setCategory(product.getCategory());
        pro.setPrice(product.getPrice());
        return productService.update(pro);
    }

    @DeleteMapping("auth/delete/{id}")
    public Product deleteProduct(@PathVariable("id") int  id) {
        return null;
    }

    @PostMapping("auth/addtocart")
    public Product addProductToCard( 
        @RequestPart("info") CartItem CartItem 
    ) {
        int idUser=1;//to do: change this laster to get id from token
        return productService.addToCartItem(CartItem,idUser);

    }
}
