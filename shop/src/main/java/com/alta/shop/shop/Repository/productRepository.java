package com.alta.shop.shop.Repository;

import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.alta.shop.shop.Models.*;


@Repository
public  interface ProductRepository extends JpaRepository<Product, Integer>  {
    public List<Product[]> findByCategory(Category ca);
    public Product findBySlug(String slug);
}