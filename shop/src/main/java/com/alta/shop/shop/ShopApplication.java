package com.alta.shop.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@SpringBootApplication
public class ShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/api/**").allowedOrigins("http://localhost:4200")
				.allowedMethods("POST","GET","DELETE","PUT");
			}
		};
	}


	@Configuration
    public class WebConfig implements WebMvcConfigurer {      
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/**")
            .addResourceLocations("classpath:/upload/")
            .setCachePeriod(0);
        }
    }

}


// ________   ________          ________  ___  ___  ________          ________  ___       ________     
//|\   ___  \|\   __  \        |\   __  \|\  \|\  \|\   ____\        |\   __  \|\  \     |\_____  \    
//\ \  \\ \  \ \  \|\  \       \ \  \|\ /\ \  \\\  \ \  \___|        \ \  \|\  \ \  \     \|___/  /|   
// \ \  \\ \  \ \  \\\  \       \ \   __  \ \  \\\  \ \  \  ___       \ \   ____\ \  \        /  / /   
//  \ \  \\ \  \ \  \\\  \       \ \  \|\  \ \  \\\  \ \  \|\  \       \ \  \___|\ \  \____  /  /_/__  
//   \ \__\\ \__\ \_______\       \ \_______\ \_______\ \_______\       \ \__\    \ \_______\\________\
//    \|__| \|__|\|_______|        \|_______|\|_______|\|_______|        \|__|     \|_______|\|_______|
//                                                                                                     
//                                                                                                     
    