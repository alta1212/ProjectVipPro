package com.alta.shop.shop.Models;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.*;

import lombok.*;
@Getter
@Setter
@Entity
@Data
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int productId;

    @NotNull(message = "title may not be null")
    @Column(length=100)
    private String title;

    @Column(length=100)
    private String metaTitle;

    private float price;
    @NotNull(message = "slug name may not be null")
    @Column(length=200,unique = true)
    private String slug;

  
    @OneToMany(mappedBy = "Product",cascade = CascadeType.ALL)
    
    private List<ProductImage> ProductImage;
    
    @ManyToOne
    @JsonBackReference
    
    private Category category ;
}

