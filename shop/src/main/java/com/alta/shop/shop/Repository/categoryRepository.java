package com.alta.shop.shop.Repository;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.alta.shop.shop.Models.*;


@Repository
public  interface CategoryRepository extends JpaRepository<Category, Integer>  {
 
}