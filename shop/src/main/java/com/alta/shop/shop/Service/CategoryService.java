package com.alta.shop.shop.Service;


import java.util.List;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alta.shop.shop.Models.*;
import com.alta.shop.shop.Repository.*;
@Service

public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public Category getByid(int id)
    {
        return categoryRepository.findById(id).orElseThrow(
            () -> new UsernameNotFoundException("Category not found with id : " + id)
        );
    }
}
