package com.alta.shop.shop.Models;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Collection;
import java.util.List;

import lombok.*;

@Setter
@Entity
@Data
@Table(name = "users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userId")
    private int userId;

    @NotNull(message = "Name may not be null")
    @Column(length=100)
    private String firstName;

    @Column(length=100)
    private String image;
    
    @Column(length=100)
    private String middleName;

    @NotNull(message = "Last name may not be null")
    @Column(length=100)
    private String lastName;

    @NotNull(message = "Phone number may not be null")
    @Column(length=100,unique = true)
    private String mobile;

    @Column(length=100,unique = true)
    private String email;

    @NotNull(message = "Password may not be null")
    @Column(length=100)
    private String passwordHash;

    @NotNull(message = "registered date may not be null")
    @Column(length=400)
    private String registeredAt;

    @Column(length=400)
    private String lastLogin;

    
    @OneToMany(mappedBy = "user")
    private List<CartItem> cartItem;

    @Transient
    private String token;
  
}

