package com.alta.shop.shop.Models;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.*;

import lombok.*;
@Getter
@Setter
@Entity
@Data
@Table(name = "ProductImage")
public class ProductImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int productimgId;
    @Column(length=400)
    private String img;

    @ManyToOne
    @JsonIgnore

    private Product Product ;
}

