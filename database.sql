CREATE TABLE users (
  id int IDENTITY(1,1)  PRIMARY Key,
  firstName varchar(50)  ,
  middleName varchar(50)  ,
  lastName varchar(50)  ,
  mobile varchar(15)  ,
  email varchar(50)  ,
  passwordHash varchar(5000)  ,
  registeredAt datetime ,
  lastLogin datetime

) 

CREATE TABLE product (
  id int IDENTITY(1,1)  PRIMARY Key,
  title varchar(75)  ,
  metaTitle varchar(100)  ,
  slug varchar(100)  ,
  createdAt datetime ,
  img1  varchar(100)  ,
  img2  varchar(100)  ,
  img3  varchar(100)  ,
  img4  varchar(100)  ,
  img5  varchar(100)  
 
) 



CREATE TABLE cart_item (
  id int IDENTITY(1,1)  PRIMARY Key,
  productId int FOREIGN KEY (productId) REFERENCES product(id),
  userId int FOREIGN KEY (userId) REFERENCES users(id),
  price float  ,
  quantity int  ,
  active int  ,
  createdAt datetime ,
  updatedAt datetime ,

)


CREATE TABLE category (
  id int IDENTITY(1,1)  PRIMARY Key,
  parentId int   FOREIGN KEY (parentId) REFERENCES category(id), 
  title varchar(75)  ,
  metaTitle varchar(100)  ,
  slug varchar(100)  ,
  content text 
 
) 
CREATE TABLE orders (
  id  int IDENTITY(1,1)  PRIMARY Key,
  usersId  int FOREIGN KEY (usersId) REFERENCES users(id),
  sessionId varchar(100)  ,
  token varchar(100)  ,
  status int  ,
  subTotal float  ,
  itemDiscount float  ,
  tax float  ,
  shipping float  ,
  total float  ,
  promo varchar(50)  ,
  discount float  ,
  grandTotal float  ,
  firstName varchar(50)  ,
  middleName varchar(50)  ,
  lastName varchar(50)  ,
  mobile varchar(15)  ,
  email varchar(50)  ,
  line1 varchar(50)  ,
  line2 varchar(50)  ,
  city varchar(50)  ,
  province varchar(50)  ,
  country varchar(50)  ,
  createdAt datetime ,
  updatedAt datetime ,
  content varchar(50)  ,
)
CREATE TABLE orders_item (
  id int IDENTITY(1,1)  PRIMARY Key,
  productId int FOREIGN KEY (productId) REFERENCES product(id), 
  ordersId int FOREIGN KEY (ordersId) REFERENCES orders(id),
  sku varchar(100)  ,
  price float  ,
  discount float  ,
  quantity int  ,
  createdAt datetime ,
  updatedAt datetime ,
  content text ,
) 
CREATE TABLE product_category (
  productId int FOREIGN KEY (productId) REFERENCES product(id),
  categoryId int FOREIGN KEY (categoryId) REFERENCES category(id),
 
)

CREATE TABLE product_review (
  id int IDENTITY(1,1)  PRIMARY Key,
  productId int FOREIGN KEY (productId) REFERENCES product(id),
  parentId  int FOREIGN KEY (parentId) REFERENCES product_review(id), 
  title varchar(100)  ,
  rating float  ,
  published int  ,
  createdAt datetime ,
  publishedAt datetime ,
  content text ,
 
) 

CREATE TABLE tag (
  id  int IDENTITY(1,1)  PRIMARY Key,
  title varchar(75)  ,
  metaTitle varchar(100)  ,
  slug varchar(100)  ,
  content text 
  
) 
CREATE TABLE product_tag (
  productId int FOREIGN KEY (productId) REFERENCES product(id),
  tagId int FOREIGN KEY (tagId) REFERENCES tag(id),
 
) 
CREATE TABLE transactions (
  id  int IDENTITY(1,1)  PRIMARY Key,
  usersId int   FOREIGN KEY (usersId) REFERENCES users(id), 
  ordersId int   FOREIGN KEY (ordersId) REFERENCES orders(id), 
  code varchar(100)  ,
  type int  ,
  mode int  ,
  status int  ,
  createdAt datetime ,
  updatedAt datetime ,
  content text 
 
)
